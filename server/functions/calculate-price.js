const calculatePrice = (basePrice) => {

    let now = new Date();
    let currentDay = now.getDay();
    let currentHours = now.getHours();
    
    if (currentDay == 6 || currentDay == 0){
        basePrice *= 1.25;
    }

    if(currentHours < 12){
        basePrice *= 1.25;
    }

    return basePrice;
}

module.exports = calculatePrice;