const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const flightSchema = new Schema({

    originPlace: {
        type: String,
        required: true
    },
    destinationPlace: {
        type: String,
        required: true
    },
    departureTime: {
        type: Date,
        required: true
    },
    basePrice: {
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('Flight', flightSchema);