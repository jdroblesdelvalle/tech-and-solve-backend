const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const reservationSchema = new Schema({

    flight: {
        type: Schema.Types.ObjectId,
        ref: 'Flight',
        required: [
            true,
            'El vuelo es obligatorio'
        ]
    },
    customerId: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    birthdate: {
        type: Date,
        required: true
    },
    paidValue: {
        type: Number,
        required: true
    },
    date: {
        type: Date,
        required: true
    }
});

module.exports = mongoose.model('Reservation', reservationSchema);