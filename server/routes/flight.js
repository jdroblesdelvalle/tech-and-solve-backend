const express = require('express');
const app = express();

const Flight = require('../models/flight');
const calculatePrice = require('../functions/calculate-price');

app.get('/', (req, res) => {

    Flight.find((err, result) => {

        if(err) {
            
            return res.send({
                ok: false,
                message: 'Ocurrió un problema inesperado, intente nuevamente.',
                err
            });
        }
        
        res.send({
            ok: true,
            flights: result.map((item) => {
                item['basePrice'] = calculatePrice(item.basePrice);
                return item;
            })
        });
    });
});

app.post('/', (req, res) => {

    let body = req.body;
    let flight = new Flight(body);

    flight.save((err, result) => {

        if(err) {
            
            return res.send({
                ok: false,
                message: 'Ocurrió un problema inesperado, intente nuevamente.',
                err
            });
        }

        res.send({
            ok: true,
            flight: result
        });
    });
});

module.exports = app;