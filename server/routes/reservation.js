const express = require('express');
const app = express();

const Reservation = require('../models/reservation');
const Flight = require('../models/flight');
const calculatePrice = require('../functions/calculate-price');

app.get('/', (req, res) => {

    const customerId = req.query.customerId;

    if (!customerId || customerId == ''){
        return res.send({
            ok: false,
            message: 'Ingrese un número de identificación.'
        });
    }

    Reservation.find({customerId}).populate('flight').exec((err, result) => {

        if (err) {
            return res.send({
                ok: false,
                message: 'Ocurrió un problema inesperado, intente nuevamente.',
                err
            });
        }

        res.send({
            ok: true,
            reservations: result
        });
    });
    
});

app.post('/', (req, res) => {

    const body = req.body;
    body.date = new Date();

    //check age
    const now = new Date(new Date().toISOString().slice(0,10));
    const birthdate = new Date(body.birthdate);
    const adultDate = new Date(birthdate).setFullYear(Number(birthdate.getFullYear()) + 18);

    if (adultDate > now){
        return res.send({
            ok: false,
            message: 'Usuario debe ser mayor de edad para realizar reservación.'
        });
    }

    //Check reservation for today
    let todayDate = new Date(new Date().toISOString().slice(0,10));
    query = {
        date: {
            $gte: todayDate,
            $lt: new Date(todayDate).setDate(todayDate.getDate() + 1)
        },
        customerId: body.customerId
    }

    Reservation.find(query, (err, reservations) => {
        if(err){
            return res.send({
                ok: false,
                message: 'Ocurrió un problema inesperado, intente nuevamente.',
                err
            });
        }

        if(reservations.length > 0){
            return res.send({
                ok: false,
                message: 'Solo se permite una reservación por día.'
            });
        }

        //Retrieve flight price
        Flight.findById(body.flight, (err, flight) => {

            if(err){
                return res.send({
                    ok: false,
                    message: 'Ocurrió un problema inesperado, intente nuevamente.',
                    err
                });
            }

            if(!flight){
                return res.send({
                    ok: false,
                    message: 'El vuelo no se encuentra disponible.',
                    err
                });
            }

            body['paidValue'] = calculatePrice(flight.basePrice);
            const reservation = new Reservation(body);

            reservation.save((err, result) => {

                if(err){
                    return res.send({
                        ok: false,
                        message: 'Ocurrió un problema inesperado, intente nuevamente.',
                        err
                    });
                }
        
                res.send({
                    ok: true,
                    reservation: result
                });
        
            });
        });
    });
    
});

module.exports = app;