var express = require('express');
var app = express();

app.use('/test', require('./test'));
app.use('/flight', require('./flight'));
app.use('/reservation', require('./reservation'));

module.exports = app;