require('./config/config');

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');

app.use(cors());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use(require('./routes/index'));

mongoose.connect(process.env.URLDB, (err) => {
    if (err) {
        console.log("DB error!");
        throw err;
    }

    console.log("DB connected!");
});

app.listen(process.env.PORT, () => {
    console.log("Escuchando el puerto ", process.env.PORT);
});